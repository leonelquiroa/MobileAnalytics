/* 
 * File:   Split.cpp
 * Author: mario
 * 
 * Created on August 28, 2018, 3:43 PM
 */

#include "Split.h"

cv::Mat Split::sliceMat(cv::Mat L,int dim,std::vector<int> _sz)
{
    cv::Mat M(L.dims - 1, std::vector<int>(_sz.begin() + 1, _sz.end()).data(), CV_8UC1, L.data + L.step[0] * 0);
    return M;
}

Split::Split(){}

Split::Split(const char* fileName) 
{
    
    baseFolder = tool.getFileName("video1.mp4");
    LogFile.open ("Log." + baseFolder + "." + tool.currentDate() + ".txt");
    folderName = tool.createFolder(baseFolder,"Gray");
    cv::VideoCapture capture(fileName); 
    if (ValidVideo(capture))
    {
        while (1)
        {
            capture >> frame;
            if (InvalidFrame(frame)) break;
            bwFrame = ToGray(frame);
            if (saveFrame(bwFrame, NoFrames)) break;
            NoFrames++;
        }
        LogFile << tool.currentTime() + " - " + std::to_string(NoFrames) + " frames extracted\n";
        LogFile.close();
    }
}
bool Split::ValidVideo(cv::VideoCapture capt) 
{
    if (capt.isOpened())
    {
        LogFile << tool.currentTime() + " - " + "Open video file - " + folderName + "\n";
        return true;
    }
    else
    {
        LogFile << tool.currentTime() + " - " + "Cannot open video file - " + folderName + "\n";
        LogFile.close();
        return false;
    }
}
bool Split::InvalidFrame(cv::Mat fram) 
{
    if (fram.empty())
    {
        LogFile << tool.currentTime() + " - " + "Completed the frame extraction from video file\n";
        return true;
    }
    else
        return false;
}
cv::Mat Split::ToGray(cv::Mat fram) 
{
    cv::Mat bwFram;
    cv::cvtColor(fram, bwFram, cv::COLOR_RGB2GRAY); // convert RGB frame to GRAY
    return bwFram;
}
bool Split::saveFrame(cv::Mat fram, int NoFram) 
{
    try {
        cv::imwrite(folderName + "/f" + std::to_string(NoFram) + ".png", fram);
        return false;
    }
    catch (runtime_error& ex) {
        LogFile << tool.currentTime() + " - " + "Exception saving gray image: %s\n", ex.what();
        LogFile.close();
        return true;
    }
}