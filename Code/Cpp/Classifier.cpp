/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Classifier.cpp
 * Author: mario
 * 
 * Created on August 29, 2018, 6:34 PM
 */

#include "Classifier.h"

Classifier::Classifier(int NoFrames, string baseFolder, string folderGray) 
{
    openFiles(baseFolder, NoFrames);
    for(int f = 0; f < NoFrames; f++)
    {
        cv::Mat result = extractDifferences(folderGray, f);
        float percMovement = getPercentage(result);
        float bins = getNoBins(result);
        classifyVec.push_back(classifyTable(f,percMovement,bins,'S'));
    }
    addTransitionFrames(classifyVec);    
    closeFiles();
    clearVectors();
}
void Classifier::openFiles(string baseFolder, int NoFrames)
{
    LogFile.open ("Log." + baseFolder + "." + tool.currentDate() + ".txt", std::ofstream::out | std::ofstream::app);
    TableFile.open("Table." + baseFolder + "." + tool.currentDate() + ".txt");
    LogFile << tool.currentTime() + " - " + std::to_string(NoFrames-1) + " differences\n";
}
cv::Mat Classifier::extractDifferences(string folderGray, int f)
{
    cv::Mat prev = cv::imread(folderGray+ "/f" + std::to_string(f-1) + ".png", CV_LOAD_IMAGE_GRAYSCALE);
    cv::Mat curr = cv::imread(folderGray+ "/f" + std::to_string(f) + ".png", CV_LOAD_IMAGE_GRAYSCALE);
    return curr - prev;
}
float Classifier::getPercentage(cv::Mat result)
{
    return ((float)cv::countNonZero(result)/(float)result.total())*100;
}
float Classifier::getNoBins(cv::Mat result)
{
    // Establish the number of bins
    int histSize = 256;
    // Set the ranges ( for B,G,R) )
    float range[] = { 0, 255 } ; const float* histRange = { range };
    //
    bool uniform = true; bool accumulate = false;
    // Histogram
    cv::Mat hist;
    cv::calcHist( &result, 1, 0, cv::Mat(), hist, 1, &histSize, &histRange, uniform, accumulate );
    //normalization
    return ((float)cv::countNonZero(hist)/(float)histSize)*100;
}
void Classifier::addTransitionFrames(vector<classifyTable> classifyVec)
{
    extractColumnsAvg(classifyVec);
    float perAvg = std::accumulate(perVec.begin(), perVec.end(), 0.0)/perVec.size();
    float binAvg = std::accumulate(binVec.begin(), binVec.end(), 0.0)/binVec.size();
    createTable(classifyVec, perAvg, binAvg);
}
void Classifier::extractColumnsAvg(vector<classifyTable> classifyVec)
{
    for(vector<int>::size_type ix = 0; ix != classifyVec.size(); ix++)
    {
        perVec.push_back(classifyVec[ix].perChanges);
        binVec.push_back(classifyVec[ix].bins);
    }
}
void Classifier::createTable(vector<classifyTable> classifyVec, float perAvg, float binAvg)
{
    for(vector<int>::size_type ix = 0; ix != classifyVec.size(); ix++)
    {
        if(classifyVec[ix].perChanges > perAvg && classifyVec[ix].bins > binAvg)
            classifyVec[ix].type = 'T';
        //store in a file
        TableFile << classifyVec[ix].NoFrame << "," << classifyVec[ix].type << endl;
    }
}
void Classifier::closeFiles()
{
    TableFile.close();
    LogFile.close();
}
void Classifier::clearVectors()
{
    classifyVec.clear();
    perVec.clear();
    binVec.clear();
}