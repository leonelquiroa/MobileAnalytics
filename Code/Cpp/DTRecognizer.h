
#include <string>
#include <sstream>
#include <opencv2/opencv.hpp>
#include "IPoperations.h"
#include "SVMoperations.h"

using namespace std;
using namespace cv;

class DTRecognizer {
	IPoperations ipo;
	vector<morph> structure = { 
		morph(11,5),		// ellypse 11x11		rectangle 3x7
		morph(5,11)			// ellypse 5x5			rectangle 3x11
	};
	float percToKeep = (float)0.8;
	float minThdArea = (float)0.1;                                                                         ///param
	float maxThdArea = (float)16;
	int NoDivHist = 4;
	int thdLTP = 5;
	int connectivity = 8;
	string fileDbImages = "imageList.0418.csv";
	string fileNamePerLabel = "cropList.0418.csv";
	string fileAnalysisImage = "report.0418.csv";
	string cropFolder = "crop\\";

	string fileNameModel = "Model.Csvc.Linear.1500.1e-3.BEPENSA.csv";
	string fileLabelsPerFrame = "LabelsPerFrameTestBEPENSA.txt";
	string fileOutput = "PredictLabelBEPENSA.txt";

	public:
		void createFile();
		void createModel();
		void TestModel();
		int analyzeImage(const char* ImageToAnalyze);
	private:
		void build2Mats(Mat &MatDescriptors, Mat &MatLabels, bool IsLabel);
		void DTRecognizer::writeMatToFile(cv::Mat& m, const char* filename);
		vector<string> CroppingImages(string fileName, int ixImg);
		Mat buildMatDescriptor(vector<string> finalTable);
		int predictLabel(Mat MatDescriptors, vector<string> finalTable);
};