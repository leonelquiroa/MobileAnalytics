#include "SVMoperations.h"

SVMoperations::SVMoperations(const String & filename)
{
	model = svm_load_model(filename.c_str());
	CV_Assert(model != 0);
	prob.l = 0;
}
SVMoperations::SVMoperations(const Mat & A, const Mat & L, double a, double b)
{
	svm_parameter param = { 0 };
	param.svm_type = C_SVC;
	param.probability = 1;
	param.cache_size = 100;

	param.kernel_type = LINEAR;
	param.C = 1500;
	param.eps = 1e-3;

	//param.kernel_type = RBF;
	//param.C = a;
	//param.gamma = b;
	
	/*
	-s		svm_type			C-SVC
	-b		probability			1
	-m		cachesize			100
	-t		kernel_type			LINEAR
								RBF
	-c		cost				1
								100
	-e		epsilon				1e-3
								1e-5
	*/

	prob.l = A.rows;
	prob.y = Malloc(double, prob.l);
	prob.x = Malloc(svm_node *, prob.l);
	for (int r = 0; r<prob.l; r++) {
		prob.x[r] = make_node(A, r);
		prob.y[r] = L.at<int>(r, 0);
	}
	model = svm_train(&prob, &param);
}
void SVMoperations::saveSVMmodel(const String & filename)
{
	svm_save_model(filename.c_str(), model);
}
double SVMoperations::getLabelSVMmodel(const Mat & query)
{
	svm_node *x = make_node(query);
	double prediction = svm_predict(model, x);
	free(x);
	return prediction;
}
void SVMoperations::getProbSVMmodel(const Mat & query, Mat & result)
{
	svm_node *x = make_node(query);
	const int classes = 23;
	double prob_est[classes];
	double prediction = svm_predict_probability(model, x, prob_est);
	result.push_back(prediction);
	for (size_t i = 0; i < 23; i++)
	{
		result.push_back(prob_est[i]);
	}
	free(x);
}
