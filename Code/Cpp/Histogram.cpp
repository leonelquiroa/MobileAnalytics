/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Histogram.cpp
 * Author: mario
 * 
 * Created on September 12, 2018, 5:29 PM
 */

#include "Histogram.h"

cv::Mat Histogram::calculateHistogram(cv::Mat concatMat)
{
    //get dimensions
    int h = concatMat.rows;
    int w = concatMat.cols;
    ///Establish the number of bins
    int histSize = 256;
    ///Set the ranges ( for B,G,R) )
    float range[] = { 0, 255 } ;
    const float* histRange = { range };
    bool uniform = true;
    bool accumulate = false;
    ///divide images
    int NoDiv = 4;
    int step = h / NoDiv;
    ///concatenate histogram
    Mat concatHist, normHist;
    for (int ixHist = 0; ixHist < NoDiv; ++ixHist)
    {
        /// ROI - Rect(x,y,width,height)
        Mat ROIpiece(concatMat, Rect(0,ixHist*step,w,step));
        ///copy
        Mat piece; 
        ROIpiece.copyTo(piece);
        ///histogram
        Mat histMat;
        /*
            * images – Source arrays. They all should have the same depth, CV_8U or CV_32F , and the same size. Each of them can have an arbitrary number of channels.
            * nimages – Number of source images.
            * channels – List of the dims channels used to compute the histogram. The first array channels are numerated from 0 to images[0].channels()-1 , the second array channels are counted from images[0].channels() to images[0].channels() + images[1].channels()-1, and so on.
            * mask – Optional mask. If the matrix is not empty, it must be an 8-bit array of the same size as images[i] . The non-zero mask elements mark the array elements counted in the histogram.
            * hist – Output histogram, which is a dense or sparse dims -dimensional array.
            * dims – Histogram dimensionality that must be positive and not greater than CV_MAX_DIMS (equal to 32 in the current OpenCV version).
            * histSize – Array of histogram sizes in each dimension.
            * ranges – Array of the dims arrays of the histogram bin boundaries in each dimension. When the histogram is uniform ( uniform =true), then for each dimension i it is enough to specify the lower (inclusive) boundary L_0 of the 0-th histogram bin and the upper (exclusive) boundary U_{\texttt{histSize}[i]-1} for the last histogram bin histSize[i]-1 . That is, in case of a uniform histogram each of ranges[i] is an array of 2 elements. When the histogram is not uniform ( uniform=false ), then each of ranges[i] contains histSize[i]+1 elements: L_0, U_0=L_1, U_1=L_2, ..., U_{\texttt{histSize[i]}-2}=L_{\texttt{histSize[i]}-1}, U_{\texttt{histSize[i]}-1} . The array elements, that are not between L_0 and U_{\texttt{histSize[i]}-1} , are not counted in the histogram.
            * uniform – Flag indicating whether the histogram is uniform or not (see above).
            * accumulate – Accumulation flag. If it is set, the histogram is not cleared in the beginning when it is allocated. This feature enables you to compute a single histogram from several sets of arrays, or to update the histogram in time.
        */
        cv::calcHist(&piece, 1, 0, Mat(), histMat, 1, &histSize, &histRange, uniform, accumulate );
        ///concatenation
        concatHist.push_back(histMat);
    }
    //normalize to [1-1000]
    cv::normalize(concatHist, normHist, 1, 1000, NORM_MINMAX, -1, Mat());
    return normHist;
}

