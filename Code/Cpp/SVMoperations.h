#include "svm.h"
#include <opencv2/opencv.hpp>
#include <string>

#define Malloc(type,n) (type *)malloc((n)*sizeof(type))

using namespace cv;

class SVMoperations {
	static svm_node *make_node(const Mat &A, int r = 0) {                             // for a single row opencv feature
		const float *dataPtr = A.ptr<float>(r);                                     // Get data from OpenCV Mat
		svm_node *x_space = Malloc(svm_node, A.cols + 1);                             // one more for the terminator
		for (int c = 0; c<A.cols; c++) {
			x_space[c].index = c + 1;
			x_space[c].value = dataPtr[c];
		}
		x_space[A.cols].index = -1;                                                 // End of sequence
		return x_space;
	}
	svm_model *model;
	svm_problem prob;

public:
	SVMoperations(const String &filename);
	SVMoperations(const Mat &A, const Mat &L, double a, double b);
	void saveSVMmodel(const String &filename);
	double getLabelSVMmodel(const Mat & query);
	void getProbSVMmodel(const Mat &query, Mat &result);
};