/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Descriptor.h
 * Author: mario
 *
 * Created on August 29, 2018, 6:35 PM
 */

#ifndef DESCRIPTOR_H
#define DESCRIPTOR_H

#include <opencv2/opencv.hpp>
#include "Tools.h"
#include <fstream>          //file
#include <sstream>


using namespace std;

class Descriptor {
    
    Tools tool;
    
    public:
        Descriptor(string baseFolder, string folderGray);
        void writeFileDescriptor(cv::Mat ListDescriptors, string baseFolder);
        void writeFileLabel(cv::Mat ListLabels, string baseFolder);
    private:

};

#endif /* DESCRIPTOR_H */

