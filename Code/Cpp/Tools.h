/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Tools.h
 * Author: mario
 *
 * Created on August 29, 2018, 10:51 AM
 */

#ifndef TOOLS_H
#define TOOLS_H
    
#include <sys/stat.h>       //mkdir
#include <iostream>         //string
#include <stdio.h>          //strtok
#include <string.h>

using namespace std;

class Tools {
    public:
        string getFileName(const char* fileName);
        string createFolder(string fileName, string postFix);
        string currentDate();
        string currentTime();
};

#endif /* TOOLS_H */

