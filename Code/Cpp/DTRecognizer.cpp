#include "DTRecognizer.h"

void DTRecognizer::createFile()
{
	ipo.cleanFile();
	cout << "*** start cropping ***" << endl;
	int ixImg = 0;

	string line;
	ifstream myfile1(fileDbImages);
	if (myfile1.is_open())
	{
		while (getline(myfile1, line))
		{
			vector<string> cropImages = CroppingImages(line, ixImg);
			ipo.writeFile(cropImages);
			++ixImg;		
		}
		myfile1.close();
	}
	cout << "*** end cropping ***" << endl;
}

void DTRecognizer::createModel()
{
	Mat MatDescriptors, MatLabels;
	build2Mats(MatDescriptors, MatLabels, true);

	vector<double> eps = { 1.0e-3, 1.0e-5, 1.0e-7, 1.0e-9 };
	ofstream fout("analysis0904.txt");
	//for (double cost = 1; cost < 2000;)
	//{
		//for (int ixeps = 0; ixeps < eps.size(); ixeps++)
		//{
			//eps[ixeps]
			///svm
			SVMoperations model(MatDescriptors, MatLabels, 0.0, 0.0); // from opencv data
			model.saveSVMmodel(fileNameModel);
			cout << "*** end building model ***" << endl;
			///
			int success = 0;
			cout << "Total elements to test " << MatDescriptors.rows << endl;
			for (int ixDesc = 0; ixDesc < MatDescriptors.rows; ixDesc++)
			{
				cout << " - " << ixDesc;
				//double predictLabel = model.getLabelSVMmodel(MatDescriptors.row(ixDesc));
				Mat probMat;
				model.getProbSVMmodel(MatDescriptors.row(ixDesc), probMat);
				double predictLabel = probMat.at<double>(0, 0);

				int realLabel = MatLabels.at<int>(ixDesc, 0);
				if ((int)predictLabel == realLabel)
					success++;
			}
			cout << endl;
			//cout << "success rate: " << ((float)success / (float)MatDescriptors.rows) * 100 << " %" << endl;
			fout << "1500," << ((float)success / (float)MatDescriptors.rows) * 100 << "%" << endl;
			//fout << cost << "," << ((float)success / (float)MatDescriptors.rows) * 100 << "%" << endl;
		//}
		//cost = cost + 500;
	//}
	fout.close();


	

	
}

void DTRecognizer::TestModel()
{
	cout << "*** create the two matrix ***" << endl;
	Mat MatDescriptors, MatLabels;
	build2Mats(MatDescriptors, MatLabels, false);
	///svm
	cout << "loading the model " << endl;
	SVMoperations model(fileNameModel); // from opencv data
	int success = 0;
	cout << "Total elements to test " << MatDescriptors.rows << endl;
	ofstream fout(fileOutput);
	for (int ixDesc = 0; ixDesc < MatDescriptors.rows; ixDesc++) 
	{
		cout << " - " << ixDesc;
		Mat probMat;
		model.getProbSVMmodel(MatDescriptors.row(ixDesc), probMat);
		double predictLabel = probMat.at<double>(0, 0);
		int frameNo = MatLabels.at<int>(ixDesc, 0);
		fout << frameNo << "," << predictLabel << endl;
	}
	fout.close();
	cout << "end prediction: " << endl;
}


///private

void DTRecognizer::build2Mats(Mat &MatDescriptors, Mat &MatLabels, bool IsLabel)
{
	cout << "*** feature extraction ***" << endl;
	string lineSummary, lineHistogram;
	///summary file
	ifstream fileSummary(fileLabelsPerFrame);
	if (fileSummary.is_open())
	{
		int fileNo = 1;
		while (getline(fileSummary, lineSummary))
		{
			istringstream splitSummary(lineSummary);
			int counter = 1;
			string folder = "";
			int label = -1;
			int frameBegin = -1;
			int frameEnd = -1;
			///split summary line
			while (!splitSummary.eof())
			{
				string x;               
				getline(splitSummary, x, ',');
				switch (counter)
				{
					case 1:
						folder = x;
					break;
					case 2:
						label = stoi(x);
					break;
					case 3:
						frameBegin = stoi(x)-1;
					break;
					case 4:
						frameEnd = stoi(x)+1;
					break;
				}
				counter++;
			}
			///histogrm file
			string histFile = "BEPENSA\\" + folder + ".Histograms.csv";
			ifstream fileHistogram(histFile);
			if (fileHistogram.is_open())
			{
				while (getline(fileHistogram, lineHistogram))
				{
					string delimiter = ",";
					int fram = stoi(lineHistogram.substr(0, lineHistogram.find(delimiter)));
					if (fram > frameBegin)
					{
						if (fram < frameEnd) 
						{
							string hist = lineHistogram.substr(lineHistogram.find(delimiter) + 1, lineHistogram.length());
							vector<float> V(224);
							istringstream splitHistogram(hist);
							///split hisogram line
							int i = 0;
							while (!splitHistogram.eof())
							{
								string x;
								getline(splitHistogram, x, ',');
								V[i] = atof(x.c_str());
								++i;
							}
							Mat Mdata = Mat(1, 224, CV_32FC1);
							memcpy(Mdata.data, V.data(), V.size() * sizeof(float));
							MatDescriptors.push_back(Mdata);

							if (IsLabel) {
								Mat row = (Mat_<int>(1, 1) << label);
								MatLabels.push_back(row);
							}
							else {
								Mat row = (Mat_<int>(1, 1) << fram);
								MatLabels.push_back(row);
							}
						}
						else
							break;
					}
					else
						continue;
				}
			}
			fileHistogram.close();
			cout << fileNo << ":" << folder << endl;
			fileNo++;
		}
		fileSummary.close();
	}
}

int DTRecognizer::analyzeImage(const char* ImageToAnalyze)
{
	cout << "*** start cropping ***" << endl;
	vector<string> cropImages = CroppingImages(ImageToAnalyze, 1);
	if (cropImages.size() == 0)
	{
		cropImages.clear();
		return 0;
	}
	cout << "*** start descriptor ***" << endl;
	Mat MatDescriptor = buildMatDescriptor(cropImages);
	if (MatDescriptor.rows == 0)
	{
		cropImages.clear();
		MatDescriptor.release();
		return 0;
	}
	cout << "*** start prediction ***" << endl;
	int value = predictLabel(MatDescriptor, cropImages);
	return value;
}


void DTRecognizer::writeMatToFile(cv::Mat& m, const char* filename)
{
	ofstream fout(filename);

	if (!fout)
	{
		cout << "File Not Opened" << endl;  return;
	}

	for (int i = 0; i<m.rows; i++)
	{
		for (int j = 0; j<m.cols; j++)
		{
			fout << m.at<float>(i, j) << "\t";
		}
		fout << endl;
	}

	fout.close();
}

vector<string> DTRecognizer::CroppingImages(string imageName, int ixImg)
{
	vector<string> finalTable;
	
	if (imageName.length() == 0)
		return vector<string>();
	
	string extension = imageName.substr(imageName.length() - 3, 3);

	//string extension = "jpg";
	Mat imgGrey = ipo.getGrayImage(imageName);
	if(imgGrey.rows == 0)
		return vector<string>();
	Mat detected_edges = ipo.getCannyImage(imgGrey);
	///morphology operators
	for (int ixStruct = 0; ixStruct < structure.size(); ++ixStruct) {
		///morphology operations
		Mat imgErode = ipo.applyMorphologyOps(detected_edges, structure[ixStruct].diamond, structure[ixStruct].rectangle);
		///labelling
		Mat labels; Mat stats; Mat centroids;
		cv::connectedComponentsWithStats(imgErode, labels, stats, centroids, connectivity, CV_16U);                         ///param
																											 ///																									 ///keep or discard crops
		ipo.classifyCrops(
			labels, stats, ixStruct, imgGrey, structure,
			extension, ixImg, finalTable,
			percToKeep, minThdArea, maxThdArea);
	}///end for structure
	return finalTable;
}

Mat DTRecognizer::buildMatDescriptor(vector<string> finalTable)
{
	Mat MatDescriptors, MatLabels;
	cout << "create matrix descriptor " << endl;
	for (int k = 0; k < finalTable.size(); ++k)
	{
		Mat hist = ipo.Descriptor(finalTable[k], NoDivHist, thdLTP);
		if (hist.rows == 0) continue;
		MatDescriptors.push_back(hist.t());
	}
	return MatDescriptors;
}

int DTRecognizer::predictLabel(Mat MatDescriptors, vector<string> finalTable)
{
	cout << "loading the model " << endl;
	try
	{
		SVMoperations model(fileNameModel); // from opencv data
		int contL1 = 0;
		int hasPhone = 0;
		ofstream myFile;
		try
		{
			myFile.open(fileAnalysisImage);
		}
		catch (const std::exception&)
		{
			cout << "Cannot load the report file" << endl;
			return 0;
		}

		cout << "calculate probabilities per crop " << endl;
		for (int ixDesc = 0; ixDesc < MatDescriptors.rows; ixDesc++) {
			Mat probMat;
			model.getProbSVMmodel(MatDescriptors.row(ixDesc), probMat);
			if (probMat.rows == 0) continue;
			double L0 = probMat.at<double>(1, 0);
			double L1 = probMat.at<double>(2, 0);
			/// L1
			if (L1>L0) {
				contL1++;
				hasPhone = 1;
				myFile << finalTable[ixDesc] << "," << to_string(L0) << "," << to_string(L1) << "," << probMat.at<double>(0, 0) << "\n";
			}
			else {
				try
				{
					remove((cropFolder + finalTable[ixDesc]).c_str());
				}
				catch (const std::exception&)
				{
					cout << "Cannot delete the crop-image" << endl;
					continue;
				}
			}
		}
		myFile << "-----------------------------------" << endl;
		myFile << "From a total of " << MatDescriptors.rows << " crops" << endl;
		myFile << contL1 << " crops have number/text in the image" << endl;
		myFile.close();
		cout << "end " << endl;
		return hasPhone;
	}
	catch (const std::exception&)
	{
		cout << "Cannot load the model" << endl;
		return 0;
	}
}

