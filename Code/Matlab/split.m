folder = '..\videos\TenVideos\';
files = [
    '030523add3e4273e38f443c3_0_screen.avi'  
    '06fe958d98fa2c03032daf25_0_screen.avi'  
    '2c774ba7fd04d66df269af79_0_screen.avi'  
    '6870b50c85a4c4288328f612_0_screen.avi'  
    '871ff85dbec59927268fba83_0_screen.avi'  
    '8b5385d49cddd4551440bf4f_0_screen.avi'  
    '9d5b2d837b18cf915bbaf3ed_0_screen.avi'  
    'd70fd5d552c4390edaec9918_0_screen.avi'  
    'e80dbad0f80c41c49f542022_0_screen.avi'  
    'fa5b72b6861e4834adefc43b_0_screen.avi'
];

for ixF = 1:size(files,1)
    name = files(ixF,:);
    system(['mkdir ' folder name(1:24)])
    vidObj = VideoReader(strcat(folder,name));
    vidHeight = vidObj.Height;
    vidWidth = vidObj.Width;
    s = struct('cdata',zeros(vidHeight,vidWidth,3,'uint8'),'colormap',[]);
    k = 1;

    while hasFrame(vidObj)
        s(k).cdata = readFrame(vidObj);
        outputFileName = sprintf('Frame %4.4d.png', k);
        imwrite(s(k).cdata, strcat(folder,name(1:24),'\',outputFileName));
        k = k+1;
    end
end

