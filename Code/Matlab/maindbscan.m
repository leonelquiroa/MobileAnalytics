load fisheriris;
% meas(150,4) -> species(150,1)
% 150 examples and 4 features -> 150 results
% 1. Sepal length in cm
% 2. Sepal width in cm
% 3. Petal length in cm
% 4. Petal width in cm

% work with only the Petal, discard the Sepal
X=meas(:,3:4);
% ------------------------------iteration#1
% parameters: linkage=ward - maxclust=8
c = clusterdata(X,'linkage','ward','maxclust', 8);
% plot
scatter(X(:,1),X(:,2),10,c);
% ------------------------------iteration#2
% parameters: linkage=complet - maxclust=4
c = clusterdata(X,'linkage','complete','maxclust', 4);
% plot
scatter(X(:,1),X(:,2),10,c);
% ------------------------------iteration#3
k = 4;
[idx,ctrs] = kmeans(X, k);
% plot
scatter(X(:,1),X(:,2),10,idx)
% ------------------------------iteration#4
[idx,C] = kmeans(X,3);
scatter(X(:,1),X(:,2),10,idx);
hold on; 
scatter(C(:,1), C(:,2),50,'d','fill');
% ------------------------------iteration#5
% x - data set (m,n); m-objects, n-variables
% k - number of objects in a neighborhood of an object 
% (minimal number of objects considered as a cluster)
% Eps - neighborhood radius, if not known avoid this parameter or put []
k = 5;
Eps = [];
[class,type]=dbscan(X,k,Eps);
scatter(X(:,1),X(:,2),10,class);
% ------------------------------iteration#6
k = 10;
Eps = 50;
[class,type]=dbscan(X,k,Eps);
scatter(X(:,1),X(:,2),10,class);