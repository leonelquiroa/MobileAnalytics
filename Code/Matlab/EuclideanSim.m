function [s] = EuclideanSim(h1,h2)
    h2 = repmat(h2,size(h1,1),1);
    s = sqrt((h1 - h2).^2);
    s = sum(s,2);
end