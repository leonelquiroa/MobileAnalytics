function ...
    [DBlabels,DBcluster,TrueTable,NewList,table_0,table_1,table_2] = ...
    calculateCluster(DBhist,frameRate)

    elements = size(DBhist,1);
    DBlabels = zeros(elements,2);
    initial = DBhist(1,1);
    label = 1;

    %assign label per row
    DBlabels(1,1) = initial;
    DBlabels(1,2) = label;
    for ix = 2 : elements
        initial = DBhist(ix-1,1);
        last = DBhist(ix,1);
        if (last - initial) == 1            %same
            DBlabels(ix,1) = last;
            DBlabels(ix,2) = label;
        else                                %different
            label = label + 1;
            DBlabels(ix,1) = last;
            DBlabels(ix,2) = label;
        end
    end
    
    %calculate centers
    NoClusters = max(DBlabels(:,2));
    DBcluster = zeros(NoClusters,size(DBhist,2)-1);
    for ix = 1:NoClusters
        int_row = DBlabels(:,2)==ix;
%         int_ix = DBlabels(int_row);
        DBcluster(ix,:) = mean(DBhist(int_row,2:end));
    end
    
    %table
    TrueTable = zeros(NoClusters,NoClusters);
    for ix = 1 : size(DBcluster,1)
        dist = EuclideanSim(DBcluster(ix,:),DBcluster);
        TrueTable(:,ix) = dist';
    end
   
    % 1st iteration
    NewList = []; 
    TempList = []; 
    ixTT = 1;
    while (ixTT < size(TrueTable,1))
        cols = TrueTable(ixTT+1:end,ixTT);
        diff = cols < 0.07; % 1-7

        for ixTemp = 1:size(diff,1)
            if(diff(ixTemp) == 0)
                break;
            else
                TempList(end+1) = ixTemp + ixTT;
            end
        end

        if (size(TempList,1) > 0 )
            TempList = sort(TempList);
            oo = [ones(size(TempList))*ixTT; TempList]';
            NewList(end+1:end+size(oo,1),:) = oo;
            ixTT = TempList(end) + 1;
            TempList = [];
        else
            ixTT = ixTT + 1;
        end
    end
    
    % table_0
    labels_0 = unique(DBlabels(:,2));
    count_0  = histc(DBlabels(:,2),labels_0);
    table_0 = zeros(size(labels_0,1),4);
    for ix = 1 : size(labels_0)
        bin = DBlabels(:,2) == labels_0(ix);
        frames = DBlabels(:,1);
        minFrame = min(frames(bin));
        maxFrame = max(frames(bin));
        table_0(ix,:) = [labels_0(ix) count_0(ix) minFrame maxFrame];
    end
    
    % table_1
    table_1 = [];
    father =  NewList(:,1);
    GroupingLabel = 1;
    ix = 1;
    while (ix <= size(labels_0,1))
%     for ix = 1 : size(labels_0)
        if(sum(father == ix) > 0)   %grouping
            ixSon = NewList(father == ix,2);
            endFrame = table_0(max(ixSon),4);
            beginFrame = table_0(ix,3);
            table_1(end+1,:) = [GroupingLabel endFrame-beginFrame+1 beginFrame endFrame];
            ix = max(ixSon);
        else                        %remains
            table_1(end+1,:) = [GroupingLabel table_0(ix,2:4)]; 
        end
        GroupingLabel = GroupingLabel + 1;
        ix = ix + 1;
    end
    
    % table_2
    table_2 = [];
    RemovingLabel = 1;
    thdFrameRate = round(frameRate/5); % 30/5 = 6, keep bigger than 6
    for ix = 1 : size(table_1,1)
        if(table_1(ix,2) > thdFrameRate)        %keep
            timeBgn = round(table_1(ix,3)/frameRate);
            timeEnd = round(table_1(ix,4)/frameRate);
            table_2(end+1,:) = [RemovingLabel table_1(ix,2:4) timeBgn timeEnd (timeEnd-timeBgn)]; 
            RemovingLabel = RemovingLabel + 1;
        end
    end
    
    %calculate centers
%     NoClusters = max(table_2(:,1));
%     DBcluster = zeros(NoClusters,size(DBhist,2)-1);
%     for ix = 1:NoClusters
%         minLim = table_2(ix,3);
%         maxLim = table_2(ix,4);
%         col = DBhist(:,1);
%         ixList = (col >= minLim) & (col <= maxLim);
%         DBcluster(ix,:) = mean(DBhist(ixList,2:end));
%     end
%     
%     TrueTable = zeros(NoClusters,NoClusters);
%     for ix = 1 : size(DBcluster,1)
%         dist = EuclideanSim(DBcluster(ix,:),DBcluster);
%         TrueTable(:,ix) = dist';
%     end
    
end


