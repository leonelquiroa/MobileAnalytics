function [s] = jaccardSim(h1,h2)
    h2 = repmat(h2,size(h1,1),1);
    inter = min(h1,h2);
    inter(isnan(inter)) = 0; % reset the zero division
    down = max(h1,h2);
    down(down == 0) = 1;
    s = inter./down;
    s = sum(s,2);
end