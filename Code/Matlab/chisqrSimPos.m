function [dst] = chisqrSimPos(h1,h2)
    h2 = repmat(h2,size(h1,1),1);
	% sb = h1 - h2;       % subtraction
    %sm(sm==0) = 1;      % avoid zero div
	sm = h1 + h2;       % sum
	rt = ((h1 - h2).^2)./sm;   % ratio
	rt(isnan(rt)) = 0; % reset the zero division
	dst = sum(rt,2);
%     [~,idx] = sort(dst,1,'ascend');
end