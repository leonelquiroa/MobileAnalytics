function [lutC] = LDN(I,n)
    %get height and width
    [r,c] = size(I);
    %result of the eight mask convolution
    R = zeros(r,c,8);
    %convert to double
    I = double(I);
    %mask creation
    M = createMask(n);
    %convolution
    for i=1:8    
       R(:,:,i) = imfilter(I,M(:,:,i));    
    end
    %order the magnitude response without sign
    [~,mapD] = sort(abs(R),3,'descend'); 
    %get the two prominent
    top = mapD(:,:,1:2)-1; 
    %Prominent direction
    Dp = top(:,:,1);
    %secondary diretions
    Ds = top(:,:,2);
    %combination %000 + 000 %32,16,8 + 4,2,1
    newC = Dp.*8 + Ds; 
    %lut
    filter = [1 2 3 4 5 6 7 8 10 11 12 13 14 15 16 17 19 20 21 22 23 24 25 26 28 29 30 31 32 33 34 35 37 38 39 40 41 42 43 44 46 47 48 49 50 51 52 53 55 56 57 58 59 60 61 62];
    lut = zeros(56,1);
    %shifted to start at 1
    filter = filter + 1; 
    %look up table index
    lut(filter) = 1:length(filter);
    %delete unnecesary codes applying lut
    lutC = lut(newC+1);
end

function D = createMask(n)
    D = zeros(2*n+1,2*n+1,8);
    % give the center negative value
    D(n+1,n+1,:) = -1; 
    % each of the eight neighbors
    D(n+1,2*n+1,1) = 1;
    D(1,2*n+1,2) = 1;
    D(1,n+1,3) = 1;
    D(1,1,4) = 1;
    D(n+1,1,5) = 1;
    D(2*n+1,1,6) = 1;
    D(2*n+1,n+1,7) = 1;
    D(2*n+1,2*n+1,8) = 1;
end
