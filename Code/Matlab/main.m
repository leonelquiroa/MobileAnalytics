%%

minFrame = 1;
vidWidth = 540;
vidHeight = 960;
division = 4;
NoBins = 8*7;
frameRate = 30;
folder = 'C:\Users\leoqu\OneDrive\Documents\MobileAnalytics\videos\TenVideos';
frameFolder = {
%     '8b5385d49cddd4551440bf4f','2772'
%     '6870b50c85a4c4288328f612','3134'  
%     '9d5b2d837b18cf915bbaf3ed','3157'  
%     '2c774ba7fd04d66df269af79','3370'
    '06fe958d98fa2c03032daf25','3658'
    
%     '030523add3e4273e38f443c3'  %4,369
%     'd70fd5d552c4390edaec9918'  %5,255
%     'e80dbad0f80c41c49f542022'  %5,269
%     '871ff85dbec59927268fba83'  %5,441 
%     'fa5b72b6861e4834adefc43b'  %6,667
};

%% #1
% for ix = 1 : size(labels_0)
for ix = 1:size(frameFolder,1)
    maxFrame = str2num(frameFolder{ix,2});
    DBbkwt = getFiles(vidHeight,vidWidth,maxFrame,minFrame,strcat(folder,'\',frameFolder{ix,1},'\Frame '));
    DBdata = classifyFrames(vidHeight,vidWidth,maxFrame,minFrame,DBbkwt);
    DBhist = getHistogram(maxFrame,minFrame,vidHeight,DBdata,DBbkwt,division,NoBins);
    csvwrite(strcat('..\videos\TenVideos\',frameFolder{ix,1},'\',frameFolder{ix,1},'.Histograms.csv'),DBhist);
    [DBlabels,DBcluster,TrueTable,NewList,table_0,table_1,table_2] = calculateCluster(DBhist,frameRate);
    csvwrite(strcat('..\videos\TenVideos\',frameFolder{ix,1},'\',frameFolder{ix,1},'.csv'),table_2);
end
%% #2
% DBbkwt = getFilesByColor(vidHeight,vidWidth,maxFrame,minFrame,FramesPath);
% DBdata = classifyFramesByColor(maxFrame,minFrame,DBbkwt,vidHeight,division);