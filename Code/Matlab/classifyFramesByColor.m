function [DBdist] = classifyFramesByColor(maxFrame,minFrame,DBbkwt,vidHeight,division)
    IxBw = (maxFrame-minFrame)+1;
    frameNum = minFrame + 1;
%     input_start = 0; output_start = 0;
%     input_end = 100; output_end = 255;
%     factor = (output_end-output_start)/(input_end-input_start);
    NoBins = 256;
    DBmap = zeros(IxBw,NoBins*division+1);
    step = vidHeight / division;
    
    % color quantization
    for ix = 1 : IxBw 
        Idiff = DBbkwt(:,:,ix)*100;
%         Imap = round(output_start + factor*(Idiff - input_start));
        F = LTP(Idiff,1,5);
        HH = [];
        for div = 1:step:vidHeight
            crop = F(div:(div+step)-1,:);
            tempH = hist(crop(:),1:NoBins);
            HH = cat(2,HH,tempH);
        end
        
        DBmap(ix,:) = [frameNum HH/sum(HH)];
        frameNum = frameNum + 1;
    end
    %measure distance
    frameNum = minFrame + 1;
    DBdist = zeros(IxBw-1,2);
    for ix = 2 : IxBw
        DBdist(ix-1,1) = frameNum;
        DBdist(ix-1,2) = chisqrSimPos(DBmap(ix,2:end),DBmap(ix-1,2:end));
        DBdist(ix-1,3) = chisqrSimNeg(DBmap(ix,2:end),DBmap(ix-1,2:end));
        DBdist(ix-1,4) = cosSim(DBmap(ix,2:end),DBmap(ix-1,2:end));
        DBdist(ix-1,5) = EuclideanSim(DBmap(ix,2:end),DBmap(ix-1,2:end));
        DBdist(ix-1,6) = jaccardSim(DBmap(ix,2:end),DBmap(ix-1,2:end));
        frameNum = frameNum + 1;
    end
end