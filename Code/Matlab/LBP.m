function [C] = LBP(I,n,varargin)
% use this function if the computer crashes when using small patches for
% computing the code. It disables the optimization of the CPU
% iptsetpref('UseIPPL', false) 
weighted = false;
uniform = false;
%% Parameters
optargin = size(varargin, 2);
stdargin = nargin - optargin;

if stdargin < 1
    error('at least 1 arguments required: fcode');
end
% parse options
i = 1;
while (i <= optargin)
    if (strcmp(varargin{i}, 'weighted'))
        if (i >= optargin)
            error('argument required for %s', varargin{i});
        else
            weighted = varargin{i+1};  
            i = i + 2;
        end
    elseif (strcmp(varargin{i}, 'uniform'))
        if (i >= optargin)
            error('argument required for %s', varargin{i});
        else
            uniform = varargin{i+1}; 
            i = i + 2;
        end
    end%if
end%while
%% Double conversion
if (size(I,3) == 3), I = rgb2gray(I); end;
I = double(I);
%% Compute the mask to process LBP
D = zeros(2*n+1,2*n+1,8);
D(n+1,n+1,:) = -1; % give the center negative value
% each of the eight neighbors
D(n+1,2*n+1,1) = 1;
D(1,2*n+1,2) = 1;
D(1,n+1,3) = 1;
D(1,1,4) = 1;
D(n+1,1,5) = 1;
D(2*n+1,1,6) = 1;
D(2*n+1,n+1,7) = 1;
D(2*n+1,2*n+1,8) = 1;
%% LBP computation
[r c] = size(I);
R = zeros(r,c,8);
for i=1:8    
   R(:,:,i) = imfilter(I,D(:,:,i));    
end
% get the code bits
B = R > 0;
% convert to the code
C = zeros(r,c);
for i=1:8
   C = C + B(:,:,i).*2^(i-1);
end

%% Compute uniform code
if uniform
  filter = [0	1	2	3	4	6	7	8	12	14	15	16	24	28	30	31	32	48	56	60	62	63	64	96	112	120	124	126	127	128	129	131	135	143	159	191	192	193	195	199	207	223	224	225	227	231	239	240	241	243	247	248	249	251	252	253	254	255];
  filter = filter + 1; % shifted to start at 1
  lut = zeros(256,1);
  lut(filter) = 1:length(filter);
  C = lut(C+1);
end

%% Weight computation
if weighted
  W = mean(abs(R),3)./I;  
  C(:,:,2) = W;
end
 

end