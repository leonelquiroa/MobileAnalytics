function [DB] = getFiles(vidHeight,vidWidth,maxFrame,minFrame,FramesPath)
    Ix = 1;
    DB = zeros(vidHeight,vidWidth,(maxFrame-minFrame)+1);       % Original
    for frameNum = minFrame : maxFrame 
        zeroCount = ' ';
        if(frameNum < 10)
            zeroCount = ' 000';
        elseif(frameNum > 9 && frameNum < 100)
            zeroCount = ' 00';
        elseif(frameNum > 99 && frameNum < 1000)
            zeroCount = ' 0';
        end
        fileName = [FramesPath,zeroCount,int2str(frameNum),'.png'];
        Irgb = imread(fileName);
        Igray = rgb2gray(Irgb);
        DB(:,:,Ix) = Igray;
        Ix = Ix + 1;
    end
end