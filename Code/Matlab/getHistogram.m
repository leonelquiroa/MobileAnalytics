function [DB] = getHistogram(maxFrame,minFrame,vidHeight,DBdata,DBbkwt,division,NoBins)
    IxBw = (maxFrame-minFrame);
    DB = zeros(IxBw,NoBins*division+1);
    step = vidHeight / division;
    ixHist = 1;
    for ix = 1 : IxBw
        if(DBdata{ix,4} == 'S')
            I = DBbkwt(:,:,ix);
            F = LDN(I,1);
%             F = LTP(I,1,5);
            HH = [];
            for div = 1:step:vidHeight
                crop = F(div:(div+step)-1,:);
                tempH = hist(crop(:),1:NoBins);
                HH = cat(2,HH,tempH);
            end
            %normalize
            DB(ixHist,:) = [ix HH/sum(HH)]; 
            ixHist = ixHist + 1;
            % plot(DBhist(40,:))
        end
    end
    DB = DB(any(DB,2),:);
end