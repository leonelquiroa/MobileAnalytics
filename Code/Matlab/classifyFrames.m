function [DBdata] = classifyFrames(vidHeight,vidWidth,maxFrame,minFrame,DBbkwt)
    DBdata = cell(1,4);  
    denominador = vidHeight * vidWidth;
    DBdiff = zeros(vidHeight,vidWidth,(maxFrame-minFrame));       % Differences
    IxBw = (maxFrame-minFrame)+1;
    Ix = 1;
    frameNum = minFrame + 1;
    % calculate
    for ix = 2 : IxBw 
        Idiff = DBbkwt(:,:,ix) - DBbkwt(:,:,ix-1);
        DBdiff(:,:,Ix) = Idiff;
        perc = 1-(sum(Idiff(:)==0)/denominador);
        bins = size(unique(Idiff),1)/512;
        %storage
        DBdata{Ix,1} = frameNum; 
        DBdata{Ix,2} = perc; 
        DBdata{Ix,3} = bins;
        %index
        frameNum = frameNum + 1;
        Ix = Ix + 1;
    end
    
    % set label
    mat_perc = cell2mat(DBdata(:,2));
    mat_bins = cell2mat(DBdata(:,3));
%     thdMaxChanges = 0.1;
%     thdMaxBins = 0.25;
    thdMaxChanges = mean(mat_perc)-median(mat_perc);
    thdMaxBins = mean(mat_bins)-median(mat_bins);
    for ix = 1 : IxBw-1 
        perc = DBdata{ix,2}; 
        bins = DBdata{ix,3};
        %rule
        type = 'S';
        if (perc > thdMaxChanges && bins > thdMaxBins)
            type = 'T';
        end
        %storage
        DBdata{ix,4} = type;
    end
    
%     fileID = fopen('exp.txt','w');
%     A = cell2mat(DBdata(:,1));
%     B = cell2mat(DBdata(:,2));
%     C = cell2mat(DBdata(:,3));
%     D = cell2mat(DBdata(:,4));
%     fprintf(fileID,'%d, %1.4f, %1.4f, %c \r\n',[A,B,C,D].');
%     fclose(fileID);

    
end